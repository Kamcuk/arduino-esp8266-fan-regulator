
MAKEFLAGS = -rR
SHELL = bash
.ONESHELL:

ARD_ARGS ?=
ARD_PORT ?= $(shell [ -e /dev/ttyUSB0 ] && echo "--port /dev/ttyUSB0")

VERBOSE ?=
ifneq ($(VERBOSE),)
ARD_ARGS += -v
endif

#fqdn = esp8266com:esp8266:generic:xtal=160,vt=flash,exception=disabled,stacksmash=disabled,ssl=all,waveform=pwm,mmu=3232,non32xfer=fast,ResetMethod=nodemcu,CrystalFreq=26,FlashFreq=80,FlashMode=qio,eesz=4M1M,led=2,sdk=nonosdk_190703,ip=lm2f,dbg=Disabled,lvl=None____,wipe=none,baud=115200
fqdn = esp8266com:esp8266:generic:xtal=160,vt=flash,exception=disabled,stacksmash=disabled,ssl=all,mmu=3232,non32xfer=fast,ResetMethod=nodemcu,CrystalFreq=26,FlashFreq=80,FlashMode=qio,eesz=4M1M,led=2,sdk=nonosdk_190703,ip=lm2f,dbg=Disabled,lvl=None____,wipe=none,baud=115200
# fqdn = esp8266:esp8266:generic:xtal=160,vt=flash,exception=disabled,ssl=all,ResetMethod=ck,CrystalFreq=26,FlashFreq=80,FlashMode=qio,eesz=4M1M,led=2,sdk=nonosdk221,ip=lm2f,dbg=Disabled,lvl=None____,wipe=none,baud=921600

define ARD
	arduino-cli compile \
		-b $(fqdn) \
		--build-path $(PWD)/_build/ \
		--build-cache-path $(PWD)/_build/core/ \
		--warnings more \
		$(ARD_PORT) \
		$1 $(ARD_ARGS) $(PWD)/fanreg/
endef
# --build-properties "build.extra_flags=-std=c++17"

define esptool
	python $(firstword \
		$(wildcard ~/Arduino/hardware/esp8266com/esp8266/tools/$1) \
		$(wildcard ~/.arduino*/packages/esp8266/hardware/esp8266/*/tools/$1) \
		$(wildcard /usr/share/arduino/hardware/esp8266/tools/$1) \
	)
endef

all: build

build: _build/fanreg.ino.bin
_build/fanreg.ino.bin: $(shell find ./fanreg -type f) ~/Arduino/libraries/lib
	@set -o pipefail
	echo $(call ARD) >&2
	stdbuf -oL $(call ARD) 2>&1 | sed 's~^$(PWD)/_build/sketch/~./fanreg/~'

~/Arduino/libraries/lib:
	./third_party/kamillibc/libs/arduino/install.sh

upload_usb: build /dev/ttyUSB0
	# $(call ARD, --upload --verify)
	$(call esptool,upload.py) --chip esp8266 --port /dev/ttyUSB0 --baud 115200 --before default_reset --after hard_reset write_flash 0x0 /home/kamil/Arduino/fanreg/_build//fanreg.ino.bin
	#$(call esptool,upload.py) --chip esp8266 --port /dev/ttyUSB0 --baud 921600 version --end --chip esp8266 --port /dev/ttyUSB0 --baud 921600 write_flash 0x0 $(PWD)/_build/fanreg.ino.bin --end

get_ip:
	@avahi-resolve -n fanreg.local | awk '{print $$2}'

upload_wifi: build
	@ip=$$(make -s get_ip)
	@set -x
	$(call esptool,espota.py) --ip=$$ip --file=$(PWD)/_build/fanreg.ino.bin -d

some_info:
	avr-nm --size-sort ./_build/sketch/*.o

clean: 
	rm -rf _build

generate_coc_settings: .vim/coc-settings.json

.vim/coc-settings.json:
	@mkdir -p .vim
	# what?
	timeout 5 make VERBOSE=1 | \
		grep 'xtensa-lx106-elf-g++' | head -n1 | tr ' ' '\n' | \
		awk '/^-([cwgE]|CC|[mO].*)$/{next} /^\//{next} /^-[xo]$/{getline;next} /^"/{gsub(/^"/,"");gsub(/"$/,"")} {flags[$0]} END{ print "["; for (i in flags) print "  \"" i "\","; print "  \"-xc++\""; print "]" }' | \
		awk 'BEGIN{ print "{ \"clangd.fallbackFlags\": " }  END{print "}"}  1' | \
		jq > .vim/coc-settings.json

telnet:
	telnet $$(make -s get_ip) 23

BAUD ?= 115200
com: /dev/ttyUSB0
	( picocom -q --exit-after 10000 --imap lfcrlf --omap crcrlf -b 115200 /dev/ttyUSB0 < /dev/tty & trap "kill $$!" INT; wait )

.PHONY: build upload_usb upload_wifi some_info get_ip generate_coc_settings telnet com
