#include "common.h"
#include <Arduino.h>
#include <climits>
#include <cmath>
#include <kc.h>

class Pwm {
	static constexpr bool debug = 1;
	static constexpr unsigned freq = 32768;
	static constexpr unsigned range = 1024;

	struct PwmSetup {
		// max for esp8266 from sources
		PwmSetup() {
			analogWriteFreq(freq);
			analogWriteRange(range);
		}
	};

	const int nr;
	unsigned cur;

public:
	Pwm(unsigned gpio) : nr(gpio) {
		assert(digitalPinHasPWM(gpio));
		set_val(0);
	}

	constexpr static unsigned get_range() { return range; }

	void setup() {
		static PwmSetup _pwm_setup;
		pinMode(nr, OUTPUT);
	}

	void set(float val) { set_val(lroundf(val / 100.0f * (float)get_range())); }

	void set_val(unsigned val) {
		if (val > get_range()) {
			val = get_range();
		}
		if (debug) {
			sout() << "Pwm(" << nr << ")=" << val << "\n";
		}
		analogWrite(nr, cur = val);
	}

	unsigned get() const { return lroundf(get_val() / (float)get_range() * 100.0f); }

	unsigned get_val() const { return cur; }
};

class Tachometer : public PtPeriodic {

	struct Irq {
		volatile uint32_t _count = 0;
		uint32_t _stamp = 0;
		uint32_t _last_count = 0;

		uint32_t wrap_sub(uint32_t a, uint32_t b) {
			return a >= b ? (a - b) : (std::numeric_limits<uint32_t>::max() - (b - a));
		}
		bool _block_too_often_interrupt() {
			constexpr auto MAX_RPM_EVER = 6000ul;
			constexpr auto MIN_DELAY_US = 1000ul * 1000ul / (MAX_RPM_EVER / 60ul);
			const uint32_t now = micros();
			if (wrap_sub(now, _stamp) > MIN_DELAY_US) {
				_stamp = now;
				return true;
			}
			return false;
		}
		void interrupt() {
			++_count;
		}
		uint32_t count() {
			// volatile access
			const uint32_t count = _count;
			const uint32_t ret = wrap_sub(_count, _last_count);
			_last_count = count;
			return ret;
		}
	};

	Irq irq;
	const int nr;
	unsigned long last;
	unsigned rpm;

	unsigned long get_count() { return irq.count(); }

public:
	Tachometer(int nr) : PtPeriodic(1000), nr(nr) {
		assert(digitalPinToInterrupt(nr) != NOT_AN_INTERRUPT);
	}
	void setup(void (*interrupt_handler)()) {
		pinMode(nr, INPUT);
		last = millis();
		attachInterrupt(digitalPinToInterrupt(nr), interrupt_handler, RISING);
	}
	unsigned getRPM() const { return rpm; }
	void interrupt() { irq.interrupt(); }
	void periodic() {
		const unsigned long now = millis();
		const unsigned long count = get_count();
		const unsigned long passed = now == last ? -1 : (now - last);
		rpm = count * 60ul * 1000ul / passed;
		last = now;
		sout() << millis() << " passed=" << passed << " rpm=" << rpm / 1000 << "."
		       << rpm % 1000 << " count=" << count << "\n";
	}
};
