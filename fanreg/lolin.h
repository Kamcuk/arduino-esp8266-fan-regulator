#pragma once

constexpr unsigned LOLIN_D0_GPIO_NR = 16;
constexpr unsigned LOLIN_D1_GPIO_NR = 5;
constexpr unsigned LOLIN_D2_GPIO_NR = 4;
constexpr unsigned LOLIN_D3_GPIO_NR = 0;
constexpr unsigned LOLIN_D4_GPIO_NR = 2;
constexpr unsigned LOLIN_D5_GPIO_NR = 14;
constexpr unsigned LOLIN_D6_GPIO_NR = 12;
constexpr unsigned LOLIN_D7_GPIO_NR = 13;
constexpr unsigned LOLIN_D8_GPIO_NR = 15;
constexpr unsigned LOLIN_TX_GPIO_NR = 100;
constexpr unsigned LOLIN_RX_GPIO_NR = 101;
constexpr unsigned LOLIN_S3_GPIO_NR = 102;
constexpr unsigned LOLIN_S2_GPIO_NR = 103;
constexpr unsigned LOLIN_PWM_D2 = LOLIN_D2_GPIO_NR;
constexpr unsigned LOLIN_PWM_D5 = LOLIN_D5_GPIO_NR;
constexpr unsigned LOLIN_PWM_D6 = LOLIN_D6_GPIO_NR;
constexpr unsigned LOLIN_PWM_D8 = LOLIN_D8_GPIO_NR;

