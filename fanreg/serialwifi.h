#include "kc.h"
#include "optional.h"
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

template <
	unsigned MAX_CLIENTS = 3,
	size_t BUFFERSIZE_OUT = 400,
	size_t BUFFERSIZE_IN = 20
>
class WifiSerial {
	BufferedStream<BUFFERSIZE_OUT> _serial;

	optional<WiFiServer> server;
	std::array<WiFiClient, MAX_CLIENTS> clients;

	SBuffer<char, BUFFERSIZE_IN> _bufin;
	std::function<void(char *line)> linecallback;

	void client_welcome(WiFiClient &client) {
		client.print("Hello client from ");
		client.print(client.remoteIP());
		client.print(":");
		client.print(client.remotePort());
		client.print("\n");
		client.flush();
	}

	void accept() {
		if (server->hasClient()) {
			auto newclient = server->available();
			for (auto &client : clients) {
				if (!client) {
					client = newclient;
					client_welcome(client);
					return;
				}
			}
			newclient.stop();
		}
	}

	void handle_input(int in) {
		if (in == -1)
			return;
		const char c = in;

		if (c == '\n') {
			if (_bufin.push_back('\0') == false) {
				assert(0);
			}
			if (linecallback) {
				linecallback(_bufin.begin());
			}
			_bufin.clear();
		} else if (_bufin.push_back(c) == false || _bufin.free_size() < 1) {
			_serial.print("Line too long\n");
			_bufin.clear();
		}
	}

	void handle_output(int in) {
		if (in == -1)
			return;
		const char c = in;

		Serial.write(c);
		for (auto &client : clients) {
			client.write(c);
		}
	}

public:
	Print &serial() { return _serial; }

	void setup(unsigned port, std::function<void(char *line)> linecallback =
				      std::function<void(char *line)>()) {
		this->linecallback = linecallback;
		server = WiFiServer(port);
		server->begin();
		//server->setNoDelay(true);
	}

	void loop() {
		accept();

		handle_output(_serial.read());

		handle_input(Serial.read());
		for (auto &client : clients) {
			handle_input(client.read());
		}
	}
};

