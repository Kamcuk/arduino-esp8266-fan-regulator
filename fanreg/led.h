#include <Arduino.h>
#include "kc.h"
#include <array>

#define LED_BUILTIN 2

struct Led {
	Led() {
		pinMode(LED_BUILTIN, OUTPUT);
	}
	void set(bool val) {
		digitalWrite(LED_BUILTIN, val ? LOW : HIGH);
	}
};

static Led led;

class ThreadBlinkLed : public PtPeriodic {
	std::array<bool, 30> vals;
	// index currently displayed val
	size_t idx = 0;

	ThreadBlinkLed() : PtPeriodic(100) {
		set_init();
	}

public:
	static ThreadBlinkLed& instance() {
		static ThreadBlinkLed inst;
		return inst;
	}

	void set_idle() {
		idx = 0;
		std::fill(vals.begin(), vals.end(), 1);
		vals[9] = vals[19] = vals[29] = 0;
	}

	void set_init() {
		idx = 0;
		bool val = false;
		for (auto& i : vals) {
			i = val;
			val = !val;
		}
	}

	void periodic() override {
		led.set(vals[idx]);
		if (++idx == vals.size()) {
			idx = 0;
			set_idle();
		}
	}
};
