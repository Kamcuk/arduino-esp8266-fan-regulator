#include "kc.h"
#include "led.h"
#include "lolin.h"
#include "ota.h"
#include "pwm.h"
#include "serialwifi.h"
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include "lolin.h"
#include "tempsensor.hpp"

static const char *MY_HOSTNAME = "fanreg";

struct DigitalPin {
	unsigned idx;
	DigitalPin(unsigned gpio_idx) : idx(gpio_idx) {}
	void setup() {
		pinMode(idx, OUTPUT);
		this->set(1);
	}
	void set(bool a) {
		digitalWrite(idx, a ? HIGH : LOW);
	}
};

static DigitalPin pwrswitch{LOLIN_D1_GPIO_NR};

struct WifiStatThread : PtPeriodic {
	WifiStatThread() : PtPeriodic(1000) {}
	void periodic() {
		sout() << "wifi:"
		       << " ssid=" << WiFi.SSID() << " localip=" << WiFi.localIP()
		       << " rssi=" << WiFi.RSSI() << "\n";
	}
};

struct HelloThread : PtPeriodic {
	HelloThread(unsigned long ms = 500) : PtPeriodic(ms) {}
	void periodic() { sout() << millis() << " Hello world!\n"; }
};



static OTA ota;
//static HelloThread hello{10000};
//static WifiStatThread wifistat;
static WifiSerial<> wifiserial;

static std::array<Pwm, 3> pwms{
	LOLIN_D5_GPIO_NR,
	LOLIN_D6_GPIO_NR,
	LOLIN_D7_GPIO_NR,
	//LOLIN_D8_GPIO_NR,
};
//static TempSensor<> tempsensor{LOLIN_D4_GPIO_NR};

SerialPrint sout() { return SerialPrint(wifiserial.serial()); }

static bool line_handle_pwm(const char *line, const char *match, size_t idx) {
	unsigned val;
	if (sscanf(line, match, &val) == 1) {
		if (idx < pwms.size()) {
			pwms[idx].set(val);
		} else {
			sout() << "Invalid pwm";
		}
		return true;
	}
	return false;
}

template<std::size_t MAXCMDS, std::size_t MAXTOKENS = 4>
struct SimpleCommandLine {
	struct Cmds {
		const char *cmd;
		void (*main)(int argc, char **argv);
	};

	const std::array<Cmds, MAXCMDS> cmds;
	SimpleCommandLine(std::initializer_list<Cmds> l) : cmds(l) {}

	static int tokenize(char *line, SBuffer<char *, MAXTOKENS> tokens) {
		const char delim[] = " \t\r\n";
		for (char *token = strtok(line, delim); ; token = strtok(NULL, delim)) {
			if (!tokens.push_back(token)) {
				return -1;
			}
			// smart
			if (token == NULL) {
				break;
			}
		}
		tokens.pop_back();
		return 0;
	}

	void handle_line(char *line) {
		SBuffer<char *, MAXTOKENS> tokens;
		if (tokenize(line, tokens)) {
			sout() << "Too many words\n";
			return;
		}
		sout() << "Received line: ";
		for (auto&& i : tokens) {
			sout() << '`' << i << "\'\n";
		}
		int argc = tokens.size();
		char **argv = tokens.begin();
		if (argc == 0) {
			sout() << "\n";
			return;
		}
		for (auto&& cmd : cmds) {
			if (strcmp(argv[0], cmd.cmd) == 0) {
				cmd.main(argc, argv);
			}
		}
	}
};

static bool cmd_check_args(int argc, char **argv, int shouldbe) {
	if (argc != shouldbe) {
		sout() << argv[0] << " needs " << shouldbe << " arguments\n";
		return 1;
	}
	return 0;
}

static void cmd_wifi(int argc, char **argv) {
	if (cmd_check_args(argc, argv, 2)) return;
	int val = atoi(argv[1]);
	//wifistat.set_enabled(val);
}

static void cmd_stat(int argc, char **argv) {
	if (cmd_check_args(argc, argv, 1)) return;
	byte mac[6];
	WiFi.macAddress(mac);
	sout() <<
		"wifi: macaddress: ";
	sout().printf("%02x:%02x:%02x:%02x:%02x:%02x\n", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
	sout() <<
		"wifi:       ssid: " << WiFi.SSID() << "\n"
		"wifi:    localip: " << WiFi.localIP() << "\n"
		"wifi:       rssi: " << WiFi.RSSI() << "\n"
		;
}


static void line_handler(char *line) {
	char *buf = strchr(line, '\r');
	if (buf) *buf = '\0';
	sout() << "Received line: `" << line << "'\n";

	int val;
	if (strcmp(line, "") == 0) {
		// empty
	} else if (sscanf(line, "wifi %d", &val) == 1) {
		//wifistat.set_enabled(val);
	} else if (strcmp(line, "stat") == 0) {
		byte mac[6];
		WiFi.macAddress(mac);
		sout() <<
			"wifi: macaddress: ";
		sout().printf("%02x:%02x:%02x:%02x:%02x:%02x\n", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
		sout() <<
			"wifi:       ssid: " << WiFi.SSID() << "\n"
			"wifi:    localip: " << WiFi.localIP() << "\n"
			"wifi:       rssi: " << WiFi.RSSI() << "\n"
			;
	} else if (sscanf(line, "hello %d", &val) == 1) {
		//hello.set_enabled(val);
	} else if (sscanf(line, "p %d", &val) == 1 || sscanf(line, "pwm %d", &val) == 1) {
		for (auto &pwm : pwms) { pwm.set(val); }
	} else if (line_handle_pwm(line, "a%d", 0)) {
	} else if (line_handle_pwm(line, "b%d", 1)) {
	} else if (line_handle_pwm(line, "c%d", 2)) {
	} else if (line_handle_pwm(line, "d%d", 3)) {
	} else if (sscanf(line, "u%d", &val) == 1 || sscanf(line, "usb %d", &val) == 1) {
		val = !!val;
		sout() << "Setting pwrswitch to " << val << "\n";
		pwrswitch.set(val);
	} else {
		sout().println("Unknown command");
	}
	sout() << "\n";
}

void setup() {
	pwrswitch.setup();

	Serial.begin(115200);
	Serial.println("Booting");
	WiFi.mode(WIFI_STA);
	WiFi.hostname(MY_HOSTNAME);
	WiFi.begin(ssid, password);
	while (WiFi.waitForConnectResult() != WL_CONNECTED) {
		Serial.println("Connection Failed! Rebooting...");
		delay(5000);
		ESP.restart();
	}

	ota.setup(MY_HOSTNAME);
	wifiserial.setup(23, &line_handler);
	//tempsensor.setup();
	for (auto& pwm : pwms) {
		pwm.setup();
	}
	// initial values
	for (auto& pwm : pwms) {
		pwm.set(19);
	}

	//Serial.println("Ready");
	//Serial.print("IP address: ");
	//Serial.println(WiFi.localIP());
}

void loop() {
	//hello.loop();
	//tempsensor.loop();
	ThreadBlinkLed::instance().loop();
	wifiserial.loop();
	ota.loop();
}

