#include <kc.h>
#include <DallasTemperature.h>
#include <OneWire.h>

template<typename IT>
struct ArrayPrinter {
	IT begin;
	IT end;
	const char *how;
	const char *dist;
};
template <typename T, std::size_t N>
ArrayPrinter<typename std::array<T, N>::const_iterator>
print_arr(const char *how, const std::array<T, N> &arr, const char *dist = "") {
	return ArrayPrinter<typename std::array<T, N>::const_iterator>{ arr.begin(), arr.end(), how, dist };
}
template <typename IT>
SerialPrint& operator<<(SerialPrint &t, const ArrayPrinter<IT>& p) {
	IT it = p.begin;
	if (it != p.end) {
		t.printf(p.how, *it++);
		while (it != p.end) {
			t.print(p.dist);
			t.printf(p.how, *it++);
		}
	}
	return t;
}


	
template<unsigned RESOLUTION = 12>
struct TempSensor : Pt {
	static constexpr unsigned resolution = RESOLUTION;

	// Setup a oneWire instance to communicate with any OneWire devices
	OneWire oneWire;
	// Pass our oneWire reference to Dallas Temperature sensor
	DallasTemperature ds18b20{&oneWire};

	unsigned long now;
	bool enabled = true;

	TempSensor(uint8_t gpio) : oneWire{gpio} {}

	struct Sensor {
		using addrtype = std::array<uint8_t, 8>;
		const addrtype addr;
		uint16_t val = -1;
		Sensor(const addrtype& addr) : addr(addr) {}
	};
	std::vector<Sensor> sensors;
	typename decltype(sensors)::iterator sensorsi;

	optional<std::array<uint8_t, 8>> getAddress(unsigned idx) {
		std::array<uint8_t, 8> ret;
		if (!ds18b20.getAddress(ret.begin(), idx)) {
			return {};
		}
		return ret;
	}

	unsigned long waittime() { return ds18b20.millisToWaitForConversion(resolution); }

	int16_t getTemp(const std::array<uint8_t, 8> &i) { return ds18b20.getTemp(i.data()); }

	static std::pair<unsigned, unsigned> temp_to_dec_pair(int16_t temp16) {
		return std::make_pair(temp16 >> 7, (temp16 & 127) * 1000 / 127);
	}

	static void print_address(const std::array<uint8_t, 8> &a) {
		for (auto &i : a) {
			sout().printf("%02x", i);
		}
	}

	void setup() {
		sout() << "TempSensor init\n";
		ds18b20.begin();
		ds18b20.setResolution(resolution);
		ds18b20.setWaitForConversion(false);
		ds18b20.setCheckForConversion(true);
		for (unsigned idx = 0; idx < ds18b20.getDS18Count(); idx++) {
			auto adr = getAddress(idx);
			if (!adr)
				continue;
			sout() << "temp: detected: " << print_arr("%02x", *adr) << "\n";
			sensors.push_back(*adr);
		}
		sout() << "getDS18Count=" << ds18b20.getDS18Count() << "\n";
		sout() << "getDeviceCount=" << ds18b20.getDeviceCount() << "\n";
	}

	int loop() {
		PT_BEGIN(pt());
		while (1) {
			ds18b20.requestTemperatures();
			PT_DELAY(pt(), &now, waittime());
			for (sensorsi = sensors.begin(); sensorsi != sensors.end(); sensorsi++) {
				sensorsi->val = getTemp(sensorsi->addr);
				if (enabled) {
					const auto t = temp_to_dec_pair(sensorsi->val);
					sout() << "temp: " << print_arr("%02x", sensorsi->addr) << " " << sensorsi->val << " "
						<< t.first << " " << t.second << "\n";
				}
				PT_YIELD(pt());
			}
			PT_DELAY(pt(), &now, 2000);
		}
		PT_END(pt());
	}

	int loop1() {
		PT_BEGIN(pt());
		while (1) {
			PT_DELAY_MS(pt(), &now, 1000);
			sout().printf("getDS18Count=%d\n", (int)ds18b20.getDS18Count());
			sout().printf("getDeviceCount=%d\n", (int)ds18b20.getDeviceCount());
			static unsigned idx = 0;
			for (idx = 0;; idx++) {
				PT_YIELD(pt());
				auto adr = getAddress(idx);
				if (!adr)
					break;
				sout().printf("Device %u has address ", idx);
				for (auto &i : *adr) {
					sout().printf("%02x", i);
				}
				sout().printf("\n");
			}
		}
		PT_END(pt());
	}
};


